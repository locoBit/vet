class BaseResponse {
  success: boolean;
  message: string;
  data: any;
}