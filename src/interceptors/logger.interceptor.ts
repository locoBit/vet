import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {

  private readonly logger = new Logger(LoggerInterceptor.name);

  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    const args = context.getArgs()[0];
    const handler = context.getHandler();
    const controller = context.getClass();


    this.logger.log(
      `Calling: ${controller.name}.${handler.name}() with args: ${JSON.stringify(args)}`
    );

    return next
      .handle()
      .pipe(
        tap(content => {
          this.logger.log(`response: ${JSON.stringify(content)}`)
        }),
      );
  }
}
