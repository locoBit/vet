import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "src/users/user.entity";
import { Repository } from "typeorm";
import { CreateUserRequest } from "./dtos/create-user-request.dto";
import { UpdateUserRequest } from "./dtos/update-user-request.dto";

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  findById(id: string) : Promise<User> {
    return this.userRepository
      .createQueryBuilder("user")
      .where("user.id = :id", { id })
      .getOne();
  }

  deleteById(id: string) {
    this.findById(id)
    .then((user: User) => {
      this.userRepository.remove(user);
    });
  }

  create(request: CreateUserRequest): Promise<User> {
    const user: User = new User();
    user.firstName = request.firstName;
    user.lastName = request.lastName;

    return this.userRepository.save(user);
  }

  async update(request: UpdateUserRequest): Promise<User> {
    return this.userRepository
      .save({
        id: request.id,
        firstName: request.firstName,
        lastName: request.lastName
      });
  }
}