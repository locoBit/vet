import { Controller, UseInterceptors } from '@nestjs/common';
import { LoggerInterceptor } from 'src/interceptors/logger.interceptor';
import { UserService } from 'src/users/user.service';
import { UpdateUserRequest } from './dtos/update-user-request.dto';
import { GrpcMethod } from '@nestjs/microservices';
import { Metadata, ServerUnaryCall } from '@grpc/grpc-js';
import { UserById } from './dtos/user-by-id.interface';
import { CreateUserRequest } from './dtos/create-user-request.dto';

@UseInterceptors(LoggerInterceptor)
@Controller('users')
export class UsersController {

  constructor(private userService: UserService) {}

  @GrpcMethod('UserService', 'FindAll')
  async findAll(
    metadata: Metadata,
    call: ServerUnaryCall<any, any>
    ): Promise<BaseResponse> {
    try {
      const data = await this.userService.findAll();
      return {
        data: data || [],
        message: "Success",
        success: true
      }
    } catch (error) {
      return {
        data: [],
        message: error.message,
        success: false
      }
    }
  }

  @GrpcMethod('UserService', 'FindById')
  async findById(
    req: UserById,
    metadata: Metadata,
    call: ServerUnaryCall<any, any>
    ): Promise<BaseResponse> {
      try {
        const data = await this.userService.findById(req.id);
        return {
          data: data || {},
          message: "Success",
          success: true
        }
      } catch (error) {
        return {
          data: {},
          message: error.message,
          success: false
        }
      }
  }

  @GrpcMethod('UserService', 'CreateUser')
  async create(
    req: CreateUserRequest,
    metadata: Metadata,
    call: ServerUnaryCall<any, any>
    ): Promise<BaseResponse> {
      try {
        const data = await this.userService.create(req);
        return {
          data,
          message: "Success",
          success: true
        }
      } catch (error) {
        return {
          data: {},
          message: error.message,
          success: false
        }
      }
  }

  @GrpcMethod('UserService', 'UpdateUser')
  async update(
    req: UpdateUserRequest,
    metadata: Metadata,
    call: ServerUnaryCall<any, any>
    ): Promise<BaseResponse> {
      try {
        const data = await this.userService.update(req);
        return {
          data,
          message: "Success",
          success: true
        }
      } catch (error) {
        return {
          data: {},
          message: error.message,
          success: false
        }
      }
  }

  @GrpcMethod('UserService', 'DeleteUser')
  async deleteById(
    req: UserById,
    metadata: Metadata,
    call: ServerUnaryCall<any, any>
    ) : Promise<BaseResponse> {
      try {
        await this.userService.deleteById(req.id);
        return {
          data: {},
          message: "Success",
          success: true
        }
      } catch (error) {
        return {
          data: {},
          message: error.message,
          success: false
        }
      }
  }
}