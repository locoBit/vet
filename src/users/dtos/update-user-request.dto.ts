export class UpdateUserRequest {
  id: string;
  firstName: string;
  lastName: string;
}